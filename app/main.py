import logging
import random
import time
from functools import wraps

import yaml
from telegram import ReplyKeyboardRemove
from telegram.ext import (CommandHandler, ConversationHandler, Filters,
                        MessageHandler, Updater)

from datetime import datetime
import pony.orm as pny

from models import User

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def send_action(action):
    """Sends `action` while processing func command."""

    def decorator(func):
        @wraps(func)
        def command_func(update, context, *args, **kwargs):
            context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
            return func(update, context,  *args, **kwargs)
        return command_func

    return decorator


def load_conf():
    "Load dynamic configuration"
    with open("app/env.yml", "r") as fd:
        return yaml.safe_load(fd.read())


def load_corpus(corpus):
    with open(f"data/{corpus}.yml", "r") as fd:
        return yaml.safe_load(fd.read())


conf = load_conf()

updater = Updater(token=conf["telegram_token"], use_context=True)
dp = updater.dispatcher

def start(update, context):
    user = update.effective_user
    user_link = user.link or ""
    user_first_name = user.first_name or ""
    user_last_name = user.last_name or ""
    user_full_name = user.full_name or ""
    user_language_code = user.language_code or ""
    user_name = user.name or ""
    try:
        with pny.db_session:
            _user = User(
                username=user["username"],
                name=user_name,
                first_name=user_first_name,
                last_name=user_last_name,
                full_name=user_full_name,
                user_id=user["id"],
                language_code=user_language_code,
                link=user_link,
                date_created=datetime.now()
            )
    except pny.core.TransactionIntegrityError as integrity_error:
        logger.info("pny user already registered")
        pass
    except Exception as error:
        logger.error(error)


    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="""
        I'm here to teach you 日本語,
        Options are:
        /hiragana
        /katakana
        /kanji
        """
    )


@send_action("Typing")
def hiragana(update, context):
    corpus = load_corpus("hiragana")
    romaji, hiragana = random.choice(list(corpus.items())) # this should be weighted based on frequency and not repeating the choices
    # logger.warning(dir(context))
    logger.warning(update.message.from_user.id)
    # value = update.message.text.partition(' ')[2]

    # # Store value
    # context.user_data[key] = value
    time.sleep(5)
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=hiragana
    )


def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                            reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END



conv_handler = ConversationHandler(
    entry_points=[CommandHandler('start', start)],
    states={},
    fallbacks=[CommandHandler('cancel', cancel)]
)

dp.add_handler(conv_handler)

updater.start_polling()
updater.idle()
