# Telegram Bot

To practice japanese

## Usage

    /start
    /hiragana <optional_parameter>
    /hiragana a,ka,sa
    /katakana <optional_parameter>
    /kanji <optional_parameter>
    /romaji <optional_parameter>

## Installation

    python3.8 -m venv .venv # or docker

## Other techniques

* Shadowing
* how to trick your brain into thinking that is playing and not learning?

## Notes

* `no` (の, ノ) key was replaced by `no_` because yaml is treating this key as `False`
* Can bots track user progress?
* [Japanese frequency of kanas](https://gawron.sdsu.edu/crypto/japanese_models/hir_freq.html)
* [The Super Mario Effect - Tricking Your Brain into Learning More | Mark Rober | TEDxPenn](https://www.youtube.com/watch?v=9vJRopau0g0)
