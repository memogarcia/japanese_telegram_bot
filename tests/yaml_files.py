import yaml

hiraganas = None
katakanas = None

with open("../data/hiragana.yml", "r") as fd:
    hiraganas = yaml.safe_load(fd.read())

with open("../data/katakana.yml", "r") as fd:
    katakanas = yaml.safe_load(fd.read())

# for k, v in hiraganas.items():
#     print(k, v)

# for k, v in katakanas.items():
#     print(k, v)

hi = [k for k, v in hiraganas.items()]
ka = [k for k, v in katakanas.items()]

if hi == ka:
    print("match")
else:
    print("no match")