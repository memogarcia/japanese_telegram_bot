import logging
import random
import time
from functools import wraps

import yaml
from telegram import ReplyKeyboardRemove
from telegram.ext import (CommandHandler, ConversationHandler, Filters,
                        MessageHandler, Updater)

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def send_action(action):
    """Sends `action` while processing func command."""

    def decorator(func):
        @wraps(func)
        def command_func(update, context, *args, **kwargs):
            context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
            return func(update, context,  *args, **kwargs)
        return command_func

    return decorator


def load_conf():
    "Load dynamic configuration"
    with open("app/env.yml", "r") as fd:
        return yaml.safe_load(fd.read())


def load_corpus(corpus):
    with open(f"data/{corpus}.yml", "r") as fd:
        return yaml.safe_load(fd.read())


conf = load_conf()

updater = Updater(token=conf["telegram_token"], use_context=True)
dp = updater.dispatcher


# def error(update, context):
#     "Log Errors caused by Updates."
#     logger.warning('Update "%s" caused error "%s"', update, context.error)


# def start(update, context):
#     context.bot.send_message(
#         chat_id=update.effective_chat.id,
#         text="""
#         I'm here to teach you 日本語,
#         Options are:
#         /hiragana
#         /katakana
#         /kanji
#         """
#     )


@send_action("Thinking")
def hiragana(update, context):
    corpus = load_corpus("hiragana")
    romaji, hiragana = random.choice(list(corpus.items())) # this should be weighted based on frequency and not repeating the choices
    # logger.warning(dir(context))
    logger.warning(update.message.from_user.id)
    # value = update.message.text.partition(' ')[2]

    # # Store value
    # context.user_data[key] = value

    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=hiragana
    )


def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                            reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


# start_handler = CommandHandler('start', start)
# hiragana_handler = CommandHandler('hiragana', hiragana)
# dp.add_handler(start_handler)
# dp.add_handler(hiragana_handler)
# dp.add_error_handler(error)

conv_handler = ConversationHandler(
    entry_points=[CommandHandler('a', hiragana)],
    states={},
    fallbacks=[CommandHandler('cancel', cancel)]
)

dp.add_handler(conv_handler)


# def echo(update, context):
#     logger.info(update.message.text)
#     context.bot.send_message(chat_id=update.effective_chat.id, text=update.message.text)

# echo_handler = MessageHandler(Filters.text, echo)
# dispatcher.add_handler(echo_handler)


updater.start_polling()
updater.idle()
